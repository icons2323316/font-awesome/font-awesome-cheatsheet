:source-highlighter: rouge

image::../../assets/asciidoc-examples/asciidoc-examples.png[]

== Asciidoc examples

=== Code
`inline code`

.code block
[source, shell]
----
npm install -g sass
----

=== Color text
WARNING: Colored texts work in .adoc files but not working in the generated .pdf files, so do not use them.

=== Highlight text
#using hashtags...#

=== Example
.My card
[example]
====
this is my example card

with multi line text
====


<<<